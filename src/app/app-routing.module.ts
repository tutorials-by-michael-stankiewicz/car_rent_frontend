import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { CarsComponent } from './cars/cars.component';
import { CarDetailComponent } from './car-detail/car-detail.component';
import {DateAndLocationComponent} from "./date-and-location/date-and-location.component";
import {SingInComponent} from "./sing-in/sing-in.component";
import {CarAlbumComponent} from "./car-album/car-album.component";

const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'detail/:id', component: CarDetailComponent },
  { path: 'cars', component: CarsComponent },
  { path: 'date-and-location', component: DateAndLocationComponent },
  { path: 'sing-in', component: SingInComponent },
  { path: 'car-album', component: CarAlbumComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
