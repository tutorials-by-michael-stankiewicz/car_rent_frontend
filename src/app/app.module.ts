import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CarDetailComponent } from './car-detail/car-detail.component';
import { CarsComponent } from './cars/cars.component';
import { CarSearchComponent } from './car-search/car-search.component';
import { MessagesComponent } from './messages/messages.component';
import {MatIconModule} from "@angular/material/icon";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { DateAndLocationComponent } from './date-and-location/date-and-location.component';
import {MatInputModule} from "@angular/material/input";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatNativeDateModule} from "@angular/material/core";
import {MatAutocompleteModule} from "@angular/material/autocomplete";
import { SingInComponent } from './sing-in/sing-in.component';
import { CarAlbumComponent } from './car-album/car-album.component';
import {MatCardModule} from "@angular/material/card";

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    MatIconModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    MatCardModule
  ],
  declarations: [
    AppComponent,
    DashboardComponent,
    CarsComponent,
    CarDetailComponent,
    MessagesComponent,
    CarSearchComponent,
    DateAndLocationComponent,
    SingInComponent,
    CarAlbumComponent
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
