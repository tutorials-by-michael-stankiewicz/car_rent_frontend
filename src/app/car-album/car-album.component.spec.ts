import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CarAlbumComponent } from './car-album.component';

describe('CarAlbumComponent', () => {
  let component: CarAlbumComponent;
  let fixture: ComponentFixture<CarAlbumComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CarAlbumComponent]
    });
    fixture = TestBed.createComponent(CarAlbumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
