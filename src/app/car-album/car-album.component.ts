import {Component, OnInit} from '@angular/core';
import {Car} from "../car";
import {CarService} from "../car.service";

@Component({
  selector: 'app-car-album',
  templateUrl: './car-album.component.html',
  styleUrls: ['./car-album.component.css']
})
export class CarAlbumComponent implements OnInit {
  cars: Car[] = [];

  constructor(private carService: CarService) { }

  ngOnInit(): void {
    this.getCars();
  }

  getCars(): void {
    this.carService.getCars()
      .subscribe(cars => this.cars = cars);
  }
}
