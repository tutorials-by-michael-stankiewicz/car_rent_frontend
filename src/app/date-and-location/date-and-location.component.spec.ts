import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DateAndLocationComponent } from './date-and-location.component';

describe('DateAndLocationComponent', () => {
  let component: DateAndLocationComponent;
  let fixture: ComponentFixture<DateAndLocationComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DateAndLocationComponent]
    });
    fixture = TestBed.createComponent(DateAndLocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
