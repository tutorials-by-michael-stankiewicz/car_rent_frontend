import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {Observable, startWith} from "rxjs";
import {map} from "rxjs/operators";

const today = new Date();
const month = today.getMonth();
const year = today.getFullYear();

@Component({
  selector: 'app-date-and-location',
  templateUrl: './date-and-location.component.html',
  styleUrls: ['./date-and-location.component.css']
})
export class DateAndLocationComponent implements OnInit {
  range = new FormGroup({
    start: new FormControl<Date | null>(null),
    end: new FormControl<Date | null>(null),
  });

  locationControl = new FormControl('');
  options: string[] = ['Warszawa', 'Wrocław', 'Gdańsk', 'Gdynia'];
  filteredOptions: Observable<string[]> | undefined;

  ngOnInit() {
    this.filteredOptions = this.locationControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value || '')),
    );
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }
}
